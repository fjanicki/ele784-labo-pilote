#include "FileOps.h"
#include "myioctl.h"

wait_queue_head_t inq,outq;

unsigned long ret;

struct file_operations Buf_fops = {

		.owner = THIS_MODULE,
		.open = buf_open,
		.release = buf_release,
		.read = buf_read,
		.write = buf_write,
		.unlocked_ioctl = buf_ioctl,
};

int buf_release (struct inode *inode, struct file *filp)
{
	printk(KERN_INFO "Entre dans buf_release\n");
	if (filp->f_flags == O_WRONLY)
	{

		--(BDev.numWriter);

	}
	if (filp->f_flags == O_RDONLY)
	{

		--BDev.numReader;

	}
	if (filp->f_flags == O_RDWR)
	{

		--(BDev.numWriter);
		--(BDev.numReader);
	}
	return 0;



}

int buf_open (struct inode *inode, struct file *filp)
{
	printk(KERN_INFO "Entre dans buf_open\n");
	printk(KERN_INFO "f_mode: %d",filp->f_flags);
	ret = 0;
	if (filp->f_flags == O_WRONLY)
	{
		printk(KERN_INFO "Entre dans buf_open WRONLY\n");
		(++BDev.numWriter);
		if (BDev.numWriter >1)
		{
			ret = ENOTTY;
		}

	}
	else if (filp->f_flags == O_RDONLY)
	{
		printk(KERN_INFO "Entre dans buf_open RDONLY\n");
		(++BDev.numReader);

	}
	else if (filp->f_flags == O_RDWR)
	{
		printk(KERN_INFO "Entre dans buf_open RDWR\n");
		if (BDev.numWriter >1)
		{
			ret = ENOTTY;
		}
		else
		{
			(++BDev.numWriter);
			(++BDev.numReader);
		}
	}
	return ret;
}

long buf_ioctl (struct file *filp, unsigned int cmd, unsigned long arg)
{
	int retval = 0;
	int err = 0;
	int temp = 0;
	// Debut verifications
	if(_IOC_TYPE(cmd) != MYMODULE_IOC_MAGIC) return -ENOTTY;
	if(_IOC_NR(cmd) > MYMODULE_IOC_MAX) return -ENOTTY;
	//Donnees accessibles?
	if(_IOC_DIR(cmd) & _IOC_READ)
		err = !access_ok(VERIFY_WRITE,(void __user*)arg, _IOC_SIZE(cmd));
	else if(_IOC_DIR(cmd) & _IOC_WRITE)
		err = !access_ok(VERIFY_READ, (void __user*)arg, _IOC_SIZE(cmd));

	if(err) return -EFAULT;
	// Fin Verifications


	switch(cmd)
	{
	case MYMODULE_GETNUMDATA:
		printk("Dans GETNUMDATA %d\n",BDev.numReader);
		if(Buffer.OutIdx > Buffer.InIdx)
			temp = (Buffer.OutIdx-Buffer.InIdx);
		else
			temp = Buffer.BufSize+(Buffer.OutIdx - Buffer.InIdx)-1;
		retval = __put_user(temp, (int __user *)arg);
		break;

	case MYMODULE_GETNUMREADER:
		printk("Dans GETNUMREADER: %d\n",BDev.numReader);
		retval = __put_user(BDev.numReader, (int __user *)arg);
		break;

	case MYMODULE_GETBUFSIZE:
		printk("Dans GETBUFSIZE\n");
		retval = __put_user(Buffer.BufSize, (int __user *)arg);
		break;

	case MYMODULE_SETBUFSIZE:
		if(! capable(CAP_SYS_ADMIN))
			return -EPERM;
		printk("Dans SETBUFSIZE\n");
		retval = __get_user(Buffer.BufSize, (int __user *)arg);
		break;

	default: return -ENOTTY;
	}

	return retval;
}

ssize_t buf_read (struct file *filp, char __user *ubuf, size_t count,
		loff_t *f_ops)
{
	int ret = 0;
	short j = 0;
	int sent = 0;
	int send = 0;

	if((filp->f_flags & O_NONBLOCK) == O_NONBLOCK)
	{
		if(down_trylock(&BDev.SemBuf)) return -ENOTTY;
	}
	else
	{
		if(down_interruptible(&BDev.SemBuf)) return -ENOTTY;
	}

	while((ret < count) && (Buffer.BufEmpty != 1))
	{
		for(j = 0; (j < READWRITE_BUFSIZE) && (ret<count) && (Buffer.BufEmpty != 1); j++)
		{
			BufOut(&Buffer, BDev.ReadBuf + j*sizeof(char));
			++ret;
			send = 1;
		}
		if (send == 1)
		{
			if(copy_to_user(ubuf + *f_ops, BDev.ReadBuf, j))
			{
				ret = -EFAULT;
				goto OUT;
			}
			*f_ops += j * sizeof(char);

			}
		}
	}

	OUT:
	up(&BDev.SemBuf);
	printk(KERN_INFO "Taille de count: %d\n",count);
	printk(KERN_INFO "READ FINISHED\n");
	return ret;
}

ssize_t buf_write (struct file *filp, const char __user *ubuf, size_t count,
		loff_t *f_ops)
{

	int ret = 0;
	short i = 0;
	short j = 0;
	int send = 0;

	if((filp->f_flags & O_NONBLOCK) == O_NONBLOCK)
	{
		if(down_trylock(&BDev.SemBuf)) return -ENOTTY;
	}
	else
	{
		if(down_interruptible(&BDev.SemBuf)) return -ENOTTY;
	}

	while((ret < count) && (Buffer.BufFull != 1))
	{

		for(j = 0; (j < READWRITE_BUFSIZE) && (ret<count) && (Buffer.BufFull != 1); j++)
		{
			if(copy_from_user(&BDev.WriteBuf[j], ubuf + *f_ops, 1))
			{
				ret = -EFAULT;
				goto OUT;
			}
			++ret;
			send = 1;
			*f_ops += sizeof(char);
		}
		if (send == 1)
		{
			for(i=0;i < j;++i)
			{
			BufIn(&Buffer, &BDev.WriteBuf[i]);
			}
			send = 0;
		}
	}

	OUT:
	up(&BDev.SemBuf);
	printk(KERN_INFO "Taille de count: %d\n",count);
	printk(KERN_INFO "READ FINISHED\n");
	return ret;
}
