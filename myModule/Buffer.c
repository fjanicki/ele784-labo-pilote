int BufIn (struct BufStruct *Buf, unsigned char *Data)
	{

	if (Buf->BufFull)
		return -1;


	Buf->BufEmpty = 0;
	Buf->Buffer[Buf->InIdx] = *Data;
	Buf->InIdx = (Buf->InIdx + 1) % Buf->BufSize;
	if (Buf->InIdx == Buf->OutIdx)
		Buf->BufFull = 1;

	return 0;
	}

int BufOut (struct BufStruct *Buf, unsigned char *Data)
	{

	if (Buf->BufEmpty)
		return -1;

 	 Buf->BufFull = 0;
 	 *Data = Buf->Buffer[Buf->OutIdx];
 	 Buf->OutIdx = (Buf->OutIdx + 1) % Buf->BufSize;
 	 if (Buf->OutIdx == Buf->InIdx)
	 Buf->BufEmpty = 1;

 	 return 0;
	}
