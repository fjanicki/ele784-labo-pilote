/*
 *Fonction: buf_init
 *
 *Description: initialiser le Pilote, initialiser au complet les
 *deux structures de données Buffer et BDev.
 *
 *Donc doit init: SemBuf, créer les tampons Buffer, ReadBuf et WriteBuf
 *ainsi qu’initialiser toutes les autres variables contenues dans
 *structBuffer et BDev.
 *
 *les 2 underscore avant init "__init" specifie que c'est bel et bien LE init. MM chose pour le exit.
 */
int __init buf_init(void);

/*
 *Fonction: buf_exit
 *
 *Description: Avant de désinstaller le Pilote, la fonction Exit doit s’assurer que tous les
 *tampons ont été détruits.
 *
 */
void __exit buf_exit(void);

/*
 * Nom: BufStruct
 *
 * Desctiption:
 *
 *
 */
struct BufStruct {

	unsigned int InIdx;
	unsigned int OutIdx;
	unsigned short BufFull;
	unsigned short BufEmpty;
	unsigned int BufSize;
	unsigned short *Buffer;

} Buffer;


/*
 * Nom: BDev
 *
 * Description:
 *
 * */
struct Buf_Dev {

<<<<<<< HEAD
 unsigned char *ReadBuf;
 unsigned char *WriteBuf;
 struct semaphore SemBuf;
 unsigned short numWriter;
 unsigned short numReader;
 dev_t dev;
 struct cdev cdev;
=======
	unsigned short *ReadBuf;
	unsigned short *WriteBuf;
	struct semaphore SemBuf;
	unsigned short numWriter;
	unsigned short numReader;
	dev_t dev;
	struct cdev cdev;
>>>>>>> 77ceeb31ca2f95376e5c5f89dfc2caa4255d6cda
} BDev;
