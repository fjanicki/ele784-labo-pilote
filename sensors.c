#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/poll.h>
#include <linux/namei.h>
#include <linux/mount.h>
#include <linux/sched.h>
#include <linux/file.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/wait.h>
#include <linux/device.h>
#include <linux/rcupdate.h>

#include <asm/atomic.h>
#include <asm/uaccess.h>

#include <linux/fdtable.h>

#include <linux/kthread.h>
#include <linux/wait.h>
#include <linux/semaphore.h>

#include <linux/tty.h>
#include <linux/tty_driver.h>
#include <linux/tty_flip.h>
#include <linux/console.h>
#include <linux/termios.h>
#include <linux/delay.h>

#include "sensors.h"
#include <linux/time.h> 

#include <../drivers/parrot/char/gpio2_ioctl.h>

MODULE_AUTHOR("Bruno De Kelper");
MODULE_LICENSE("GPL");

struct task_struct* tty_thread;
struct task_struct* nav_thread;

struct fasync_struct *async_queue;


static int ttygrab_thread(void* pData) {
	struct ttygrab	*ttygrab  = (struct ttygrab *) pData;
	struct ttygrab_data	*data = (struct ttygrab_data *) ttygrab->data;
	static struct file	*f_tty = NULL;
	static uint8_t		buf[2*DATASIZE];
	static mm_segment_t	old_fs;
	uint8_t	cmd = 0x01;
	int		num = 0, tp_num = 0;
	int		res = 0;

	if ((ttygrab->f_tty = filp_open("/dev/ttyO1", O_RDWR | O_NOCTTY, 0)) == NULL) {
		printk(KERN_ALERT"Bruno - %s - Unable to open /dev/ttyO1 ", __FUNCTION__);
		return -1;
	}
	f_tty = ttygrab->f_tty;

	old_fs = get_fs();
	set_fs(KERNEL_DS);

	if ((res = (f_tty->f_op->unlocked_ioctl)(f_tty, TCSETS, (unsigned long) &configs)) < 0)
		printk(KERN_ALERT"Bruno - %s - tty_mode_ioctl->TCGETS => res = %d ", __FUNCTION__, res);

	if ((res = (f_tty->f_op->write)(f_tty, &cmd, 1, NULL)) < 0)
		printk(KERN_ALERT"Bruno - %s - ttygrab.f_tty->f_op->write(cmd) => res = %d ", __FUNCTION__, res);

	set_fs(old_fs);

	{	struct sched_param param;
		param.sched_priority = 99;
		sched_setscheduler(tty_thread, SCHED_FIFO, &param);
	}

	data->ech_num_offset = 0;

	while (1) {
		if (kthread_should_stop())
			break;

		set_fs(KERNEL_DS);
		num = 0;
		buf[0] = 0;
		do {
			num = (f_tty->f_op->read)(f_tty, (char *) &(buf[0]), 2, NULL);
			if ((num > 2)||(num < 0))
				printk(KERN_ALERT"Bruno : %s - Read error #1 : num = %d ", __FUNCTION__, num);
		} while (buf[0] != 0x003A);
		while (num < 2*DATASIZE) {
			tp_num = (f_tty->f_op->read)(f_tty, (uint8_t *) &(buf[num]), 2*DATASIZE-num, NULL);
			if ((tp_num > (2*DATASIZE-num))||(tp_num < 0))
				printk(KERN_ALERT"Bruno : %s - Read error #2 : tp_num = %d ", __FUNCTION__, tp_num);
			else
				num += tp_num;
		}
		set_fs(old_fs);

		spin_lock(&data->lock_buf);
		ktime_get_ts(&(data->timestamp));
		memcpy((void *) &data->buffer[0], (void *) &(buf[0]), DATASIZE*sizeof(uint16_t));
		data->ech_num_offset += (buf[NUMSAMPLE_POS] == 0) ? MAXNUMECH : 0;
		spin_unlock(&data->lock_buf);

		atomic_inc(&ttygrab->new_sample);
		wake_up_interruptible_sync(&ttygrab->sample_wait);
	}

 	if ((res = (f_tty->f_op->release)(f_tty->f_path.dentry->d_inode, f_tty)) < 0)
		printk(KERN_ALERT"Bruno - %s - f_tty->f_op->release => res = %d ", __FUNCTION__, res);
	ttygrab->f_tty = NULL;

	return res;
}


static int navdata_thread(void* pData) {
	struct ttygrab	*ttygrab  = (struct ttygrab *) pData;
	struct ttygrab_data	*data = (struct ttygrab_data *) ttygrab->data;
	struct nav_data	*accel    = (struct nav_data *) data->navdata[0];
	struct nav_data	*gyro     = (struct nav_data *) data->navdata[1];
	struct nav_data	*sonar    = (struct nav_data *) data->navdata[2];
	struct nav_data	*barom    = (struct nav_data *) data->navdata[3];
	struct nav_data	*magneto  = (struct nav_data *) data->navdata[4];
	static uint16_t	buf[DATASIZE];
	struct timespec	timestamp;
	uint16_t	    i, CheckSum;
	int			    new_sample, validCheckSum;
	int			    res = 0;

	init_MUTEX(&(accel->ReadMutex));
	spin_lock_init(&(accel->lock));
	init_waitqueue_head(&(accel->wait));
	atomic_set(&accel->status, INVALID_CHECKSUM);

	init_MUTEX(&(gyro->ReadMutex));
	spin_lock_init(&(gyro->lock));
	init_waitqueue_head(&(gyro->wait));
	atomic_set(&gyro->status, INVALID_CHECKSUM);

	init_MUTEX(&(sonar->ReadMutex));
	spin_lock_init(&(sonar->lock));
	init_waitqueue_head(&(sonar->wait));
	atomic_set(&sonar->status, INVALID_CHECKSUM);

	init_MUTEX(&(barom->ReadMutex));
	spin_lock_init(&(barom->lock));
	init_waitqueue_head(&(barom->wait));
	atomic_set(&barom->status, INVALID_CHECKSUM);

	init_MUTEX(&(magneto->ReadMutex));
	spin_lock_init(&(magneto->lock));
	init_waitqueue_head(&(magneto->wait));
	atomic_set(&magneto->status, INVALID_CHECKSUM);

	{	struct sched_param param;
		param.sched_priority = 98;
		sched_setscheduler(nav_thread, SCHED_FIFO, &param);
	}

	while (1) {
		while (atomic_read(&(ttygrab->start)) <= 0)
			wait_event_interruptible(ttygrab->start_wait, atomic_read(&ttygrab->start) > 0);

		if (kthread_should_stop())
			break;

		new_sample = atomic_read(&ttygrab->new_sample);
		while (atomic_read(&ttygrab->new_sample) == new_sample)
			wait_event_interruptible(ttygrab->sample_wait, atomic_read(&ttygrab->new_sample) != new_sample);

		spin_lock(&data->lock_buf);
		memcpy(&(buf[0]), &data->buffer[0], DATASIZE*sizeof(uint16_t));
		memcpy(&timestamp, &(data->timestamp), sizeof(struct timespec));
		spin_unlock(&data->lock_buf);

		CheckSum = 0;
		for (i = 1; i < DATASIZE-1; i++)
			CheckSum += buf[i];
		validCheckSum = (CheckSum == buf[DATASIZE-1]) ? 1 : 0;

		if ((buf[1] % ACCEL_RATE) == ACCEL_TRIG) {
			spin_lock(&(accel->lock));
			atomic_set(&accel->status, (validCheckSum) ? NEW_SAMPLE : INVALID_CHECKSUM);
			memcpy(&(accel->timestamp), &timestamp, sizeof(struct timespec));
			accel->ech_num = (data->ech_num_offset+buf[NUMSAMPLE_POS])/ACCEL_RATE;
			memcpy(&(accel->data[0]), &(buf[ACCEL_POS]), 3*sizeof(uint16_t));
			spin_unlock(&(accel->lock));
			wake_up_interruptible_sync(&(accel->wait));
			if (waitqueue_active(&accel->async_queue)) {
				kill_fasync(&accel->async_queue, SIGIO, POLL_IN);
			}
		}
		
		if ((buf[1] % GYRO_RATE) == GYRO_TRIG) {
			spin_lock(&(gyro->lock));
			atomic_set(&gyro->status, (validCheckSum) ? NEW_SAMPLE : INVALID_CHECKSUM);
			memcpy(&(gyro->timestamp), &timestamp, sizeof(struct timespec));
			gyro->ech_num = (data->ech_num_offset+buf[NUMSAMPLE_POS])/GYRO_RATE;
			memcpy(&(gyro->data[0]), &(buf[GYRO_POS]), 3*sizeof(uint16_t));
			spin_unlock(&(gyro->lock));
			wake_up_interruptible_sync(&(gyro->wait));
			if (waitqueue_active(&gyro->async_queue)) {
				kill_fasync(&gyro->async_queue, SIGIO, POLL_IN);
			}
		}
		
		if (((buf[1] % SONAR_RATE) == SONAR_TRIG) && (buf[SONAR_POS] & 0x8000)) {
			spin_lock(&(sonar->lock));
			atomic_set(&sonar->status, (validCheckSum) ? NEW_SAMPLE : INVALID_CHECKSUM);
			memcpy(&(sonar->timestamp), &timestamp, sizeof(struct timespec));
			sonar->ech_num = (data->ech_num_offset+buf[NUMSAMPLE_POS])/SONAR_RATE;
			buf[SONAR_POS] &= 0x7fff;
			memcpy(&(sonar->data[0]), &(buf[SONAR_POS]), 3*sizeof(uint16_t));
			spin_unlock(&(sonar->lock));
			wake_up_interruptible_sync(&(sonar->wait));
			if (waitqueue_active(&sonar->async_queue)) {
				kill_fasync(&sonar->async_queue, SIGIO, POLL_IN);
			}
		}

		if ((buf[1] % BAROM_RATE) == BAROM_TRIG) {
			spin_lock(&(barom->lock));
			atomic_set(&barom->status, (validCheckSum) ? NEW_SAMPLE : INVALID_CHECKSUM);
			memcpy(&(barom->timestamp), &timestamp, sizeof(struct timespec));
			barom->ech_num = (data->ech_num_offset+buf[NUMSAMPLE_POS])/BAROM_RATE;
			barom->data[0] = buf[BAROM_POS];
			spin_unlock(&(barom->lock));
			wake_up_interruptible_sync(&(barom->wait));
			if (waitqueue_active(&barom->async_queue)) {
				kill_fasync(&barom->async_queue, SIGIO, POLL_IN);
			}
		}
		
		if ((buf[1] % MAGNETO_RATE) == MAGNETO_TRIG) {
			spin_lock(&(magneto->lock));
			atomic_set(&magneto->status, (validCheckSum) ? NEW_SAMPLE : INVALID_CHECKSUM);
			memcpy(&(magneto->timestamp), &timestamp, sizeof(struct timespec));
			magneto->ech_num = (data->ech_num_offset+buf[NUMSAMPLE_POS])/MAGNETO_RATE;
			memcpy(&(magneto->data[0]), &(buf[MAGNETO_POS]), 3*sizeof(uint16_t));
			spin_unlock(&(magneto->lock));
			wake_up_interruptible_sync(&(magneto->wait));
			if (waitqueue_active(&magneto->async_queue)) {
				kill_fasync(&magneto->async_queue, SIGIO, POLL_IN);
			}
		}

	}

	return res;
}


static int ttygrab_start(void) {
	atomic_inc(&(ttygrab.start));
	if (atomic_read(&(ttygrab.start)) == 1) {
		wake_up_interruptible_sync(&(ttygrab.start_wait));
	}
	return 0;
}


static int ttygrab_stop(void) {
	atomic_dec(&(ttygrab.start));
	if (atomic_read(&(ttygrab.start)) <= 0) {
		atomic_set(&(ttygrab.start), 0);
	}
	return 0;
}


static ssize_t sensor_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos) {
	struct sensordev *sensor = (struct sensordev *) filp->private_data;
	struct nav_data	*data  = (struct nav_data *) sensor->data;
	struct read_data  *tp;
	loff_t OffSet = 0;

	down_interruptible(&(data->ReadMutex));
	if (f_pos != NULL)
		OffSet = (loff_t) *f_pos;
	if ((OffSet + count) > sizeof(struct read_data))
		count = sizeof(struct read_data) - OffSet;
	if (count < 0) {
		up(&(data->ReadMutex));
		return -EFAULT;
	}

	if (filp->f_flags & O_NONBLOCK) {
		if (atomic_read(&(data->status)) == OLD_SAMPLE) {
			up(&(data->ReadMutex));
			return -EAGAIN;
		}
	} else {
		while (atomic_read(&(data->status)) == OLD_SAMPLE) {
			up(&(data->ReadMutex));
			wait_event_interruptible(data->wait, atomic_read(&(data->status)) != OLD_SAMPLE);
			down_interruptible(&(data->ReadMutex));
		}
	}

	spin_lock(&(data->lock));
	switch (data->type) {
	case ACCELEROMETRE :	AccelData.status	  = (int16_t) atomic_read(&(data->status));
							memcpy(&(AccelData.timestamp), &(data->timestamp), sizeof(struct timespec));
							AccelData.ech_num	  = data->ech_num;
							AccelData.type		  = data->type;
							AccelData.data[0]	  = data->data[0];
							AccelData.data[1]	  = data->data[1];
							AccelData.data[2]	  = data->data[2];
							tp = &AccelData;
							break;
	case GYROSCOPE :		GyroData.status		 = (int16_t) atomic_read(&(data->status));
							memcpy(&(GyroData.timestamp), &(data->timestamp), sizeof(struct timespec));
							GyroData.ech_num	 = data->ech_num;
							GyroData.type		 = data->type;
							GyroData.data[0]	 = data->data[0];
							GyroData.data[1]	 = data->data[1];
							GyroData.data[2]	 = data->data[2];
							tp = &GyroData;
							break;
	case SONAR :			SonarData.status	  = (int16_t) atomic_read(&(data->status));
							memcpy(&(SonarData.timestamp), &(data->timestamp), sizeof(struct timespec));
							SonarData.ech_num	  = data->ech_num;
							SonarData.type		  = data->type;
							SonarData.data[0]	  = data->data[0];
							SonarData.data[1]	  = data->data[1];
							SonarData.data[2]	  = data->data[2];
							tp = &SonarData;
							break;
	case BAROMETRE :		BaromData.status	  = (int16_t) atomic_read(&(data->status));
							memcpy(&(BaromData.timestamp), &(data->timestamp), sizeof(struct timespec));
							BaromData.ech_num	  = data->ech_num;
							BaromData.type		  = data->type;
							BaromData.data[0]	  = data->data[0];
							BaromData.data[1]	  = data->data[1];
							BaromData.data[2]	  = data->data[2];
							tp = &BaromData;
							break;
	case MAGNETOMETRE :		MagnetoData.status	    = (int16_t) atomic_read(&(data->status));
							memcpy(&(MagnetoData.timestamp), &(data->timestamp), sizeof(struct timespec));
							MagnetoData.ech_num	    = data->ech_num;
							MagnetoData.type		= data->type;
							MagnetoData.data[0]	    = data->data[0];
							MagnetoData.data[1]	    = data->data[1];
							MagnetoData.data[2]	    = data->data[2];
							tp = &MagnetoData;
							break;
	}
	atomic_set(&(data->status), OLD_SAMPLE);
	spin_unlock(&(data->lock));

    if (copy_to_user(buf, ((char *)tp)+OffSet, count)) {
		up(&(data->ReadMutex));
        return -EFAULT;
	}
	up(&(data->ReadMutex));

	return count;
}


static int sensor_poll(struct file *filp, poll_table *wait) {
	struct sensordev *sensor = (struct sensordev *) filp->private_data;
	struct nav_data	*data  = (struct nav_data *) sensor->data;
	unsigned int mask = 0;

	poll_wait(filp, &data->wait,  wait);
	if (atomic_read(&(data->status)) != OLD_SAMPLE)
            mask |= POLLIN | POLLRDNORM;

	return mask;
}



static int sensor_open(struct inode *inode, struct file *filp) {

	if (inode->i_rdev == accel_dev.dev)
		filp->private_data = &accel_dev;
	if (inode->i_rdev == gyro_dev.dev)
		filp->private_data = &gyro_dev;
	if (inode->i_rdev == sonar_dev.dev)
		filp->private_data = &sonar_dev;
	if (inode->i_rdev == barom_dev.dev)
		filp->private_data = &barom_dev;
	if (inode->i_rdev == magneto_dev.dev)
		filp->private_data = &magneto_dev;

	ttygrab_start();

	return 0;
}



static int sensor_release(struct inode *inode, struct file *filp) {

	ttygrab_stop();
	sensor_fasync(-1, filp, 0);

	return 0;
}



static int sensor_fasync(int fd, struct file *filp, int mode) {
	struct inode *inode = filp->f_path.dentry->d_inode;
	struct sensordev *sensor = (struct sensordev *) filp->private_data;
	struct nav_data	*data  = (struct nav_data *) sensor->data;
	int	ret;

	ret = fasync_helper(fd, filp, mode, &data->async_queue);
//printk(KERN_ALERT"%s (after) : fd = %u  ret = %u  fasync = %p  file = %p  mode = %d", __FUNCTION__, fd, ret, data->async_queue, filp, mode);
	return ret;
}


static int __init sensors_init(void) {
	int res;

	spin_lock_init(&(ttygrab.data->lock_buf));
	init_waitqueue_head(&(ttygrab.sample_wait));
	atomic_set(&(ttygrab.start), 0);
	init_waitqueue_head(&(ttygrab.start_wait));

	if ((tty_thread = kthread_run(ttygrab_thread, &ttygrab, "ttygrab")) <= 0)
		return -1;
	if ((nav_thread = kthread_run(navdata_thread, &ttygrab, "navdata")) <= 0)
		return -1;


	res = alloc_chrdev_region(&accel_dev.dev, DEV_MINOR, DEV_MINORS, "accel");
	accel_dev.cclass = class_create(THIS_MODULE, "accel");
	device_create(accel_dev.cclass, NULL, accel_dev.dev, NULL, "accel");
	cdev_init(&accel_dev.cdev, &sensordev_fops);
	accel_dev.cdev.owner = THIS_MODULE;
	accel_dev.cdev.ops = &sensordev_fops;
	res = cdev_add(&accel_dev.cdev, accel_dev.dev, 1);

	res = alloc_chrdev_region(&gyro_dev.dev, DEV_MINOR, DEV_MINORS, "gyro");
	gyro_dev.cclass = class_create(THIS_MODULE, "gyro");
	device_create(gyro_dev.cclass, NULL, gyro_dev.dev, NULL, "gyro");
	cdev_init(&gyro_dev.cdev, &sensordev_fops);
	gyro_dev.cdev.owner = THIS_MODULE;
	gyro_dev.cdev.ops = &sensordev_fops;
	res = cdev_add(&gyro_dev.cdev, gyro_dev.dev, 1);

	res = alloc_chrdev_region(&sonar_dev.dev, DEV_MINOR, DEV_MINORS, "sonar");
	sonar_dev.cclass = class_create(THIS_MODULE, "sonar");
	device_create(sonar_dev.cclass, NULL, sonar_dev.dev, NULL, "sonar");
	cdev_init(&sonar_dev.cdev, &sensordev_fops);
	sonar_dev.cdev.owner = THIS_MODULE;
	sonar_dev.cdev.ops = &sensordev_fops;
	res = cdev_add(&sonar_dev.cdev, sonar_dev.dev, 1);

	res = alloc_chrdev_region(&barom_dev.dev, DEV_MINOR, DEV_MINORS, "barom");
	barom_dev.cclass = class_create(THIS_MODULE, "barom");
	device_create(barom_dev.cclass, NULL, barom_dev.dev, NULL, "barom");
	cdev_init(&barom_dev.cdev, &sensordev_fops);
	barom_dev.cdev.owner = THIS_MODULE;
	barom_dev.cdev.ops = &sensordev_fops;
	res = cdev_add(&barom_dev.cdev, barom_dev.dev, 1);

	res = alloc_chrdev_region(&magneto_dev.dev, DEV_MINOR, DEV_MINORS, "magneto");
	magneto_dev.cclass = class_create(THIS_MODULE, "magneto");
	device_create(magneto_dev.cclass, NULL, magneto_dev.dev, NULL, "magneto");
	cdev_init(&magneto_dev.cdev, &sensordev_fops);
	magneto_dev.cdev.owner = THIS_MODULE;
	magneto_dev.cdev.ops = &sensordev_fops;
	res = cdev_add(&magneto_dev.cdev, magneto_dev.dev, 1);

	return 0;
}


static void __exit sensors_exit(void) {

	cdev_del(&accel_dev.cdev);
	device_destroy(accel_dev.cclass, accel_dev.dev);
	class_destroy(accel_dev.cclass);
	unregister_chrdev_region(accel_dev.dev, DEV_MINORS);

	cdev_del(&gyro_dev.cdev);
	device_destroy(gyro_dev.cclass, gyro_dev.dev);
	class_destroy(gyro_dev.cclass);
	unregister_chrdev_region(gyro_dev.dev, DEV_MINORS);

	cdev_del(&sonar_dev.cdev);
	device_destroy(sonar_dev.cclass, sonar_dev.dev);
	class_destroy(sonar_dev.cclass);
	unregister_chrdev_region(sonar_dev.dev, DEV_MINORS);

	cdev_del(&barom_dev.cdev);
	device_destroy(barom_dev.cclass, barom_dev.dev);
	class_destroy(barom_dev.cclass);
	unregister_chrdev_region(barom_dev.dev, DEV_MINORS);

	cdev_del(&magneto_dev.cdev);
	device_destroy(magneto_dev.cclass, magneto_dev.dev);
	class_destroy(magneto_dev.cclass);
	unregister_chrdev_region(magneto_dev.dev, DEV_MINORS);

	atomic_set(&(ttygrab.start), 1);
	wake_up_interruptible_all(&(ttygrab.start_wait));

	kthread_stop(nav_thread);
	kthread_stop(tty_thread);
}


module_init(sensors_init);
module_exit(sensors_exit);
