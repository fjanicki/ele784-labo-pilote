/*
 ============================================================================
 Name        : driverTester.c
 Author      : Frederic Portaria-Janicki
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "../../myModule/myioctl.h"

int main(void) {
    int fd;
    int * ret = malloc(sizeof(int));
    char ch;
    char * write_buf;
    char * read_buf;
    int i = 0;

    read_buf = malloc(100*sizeof(char));
    write_buf = malloc(100*sizeof(char));

    fd = open("/dev/etsele_cdev", O_RDWR);

    for(i=0;i<100;i++)
    {
    	read_buf[i] = 0;
    }
    if (fd == -1)
    {
            printf("Error in opening file \n");
            exit(-1);
    }
	printf("Count = %d\n", sizeof(read_buf));
    printf ("Press r to read from device or w to write the device, i for ioctl");
    scanf ("%c", &ch);


    switch (ch) {
            case 'w':
                   printf (" Enter the data to be written into device");
                    scanf (" %c", write_buf);
                    write(fd, write_buf, 100*sizeof(char));
                    break;
            case 'r':
            		read(fd, read_buf, 100*sizeof(char));
                    for(i = 0; i < 100; i++)
                    printf ("The data #%d in the device is %c\n", i, read_buf[i]);
                    break;
            case 'i':
            		ioctl(fd,MYMODULE_GETBUFSIZE,ret);
            		printf ("Output de ioctl: %d\n",*ret);
            		break;

            default:
                    printf("Wrong choice \n");
                    break;
    }
    close(fd);
    free(read_buf);
    free(write_buf);
	return EXIT_SUCCESS;
}
