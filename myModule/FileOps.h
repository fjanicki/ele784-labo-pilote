/*
 * Fonction: buf_read
 *
 * Description:
 *
 * Doit supporter les lectures bloquantes et non-bloquantes (voir
filp->f_flags).
 *
 * Protege l’accès au tampon Buffer à l’aide du sémaphore "SemBuf"
 *
 * Les données sont retirées une à une du Buffer, à l’aide de la fonction
"BufOut", et placées dans le tampon "ReadBuf", avant d’être transmises en
bloc à l’usager.
 *
 * Ce processus se répète autant de fois que nécessaire, tant qu’il y a des
données disponibles et que la requête n’est pas entièrement satisfaite
(nombre de données demandées).
 *
 * S’il manque de données pour satisfaire la requête (Buffer vide), seules
les données disponibles sont retournées à l’usager.
 *
 * En mode bloquant : bloque si le Buffer est momentanément non
disponible (voir SemBuf).
 *
 * En mode non-bloquant : retourne immédiatement un code d’erreur
(EAGAIN) à l’usager, si le Buffer est momentanément non disponible
(voir SemBuf).

 */

ssize_t buf_read (struct file *filp, char __user *ubuf, size_t count,
		loff_t *f_ops);

/*
 * Fonction: buf_write
 *
 * Description:
 *
 * Supporte écritures bloquantes/non-bloquantes (voir
 * filp->f_flags)
 *
 * Protege l’accès au tampon Buffer à l’aide du sémaphore "SemBuf"
 *
 * Les données sont récupérées en bloc et placées dans le tampon
 * "WriteBuf"
 * Elles sont ensuite transmises une à une dans le Buffer, à
 * l’aide de la fonction "BufIn"
 *
 * Ce processus se répète autant de fois que nécessaire, tant qu’il y a de la
 * place disponibles dans le Buffer et que la requête n’est pas entièrement
 * satisfaite (nombre de données fournies)
 *
 * S’il manque de la place pour satisfaire la requête (Buffer plein), seules
 * les places disponibles sont utilisées.
 *
 * En mode bloquant : bloque si le Buffer est momentanément non
 * disponible (voir "SemBuf").
 * En mode non-bloquant : retourne immédiatement un code d’erreur
 * ("EAGAIN") à l’usager, si le Buffer est momentanément non disponible
 * (voir "SemBuf").
 */

ssize_t buf_write (struct file *filp, const char __user *ubuf, size_t count,
		loff_t *f_ops);

/*
 * Fonction: buf_release
 *
 * Description: Verifie le mode d'ouverture, (O_RDONLY, O_WRONLY, O_RDWR)
 *
 * Si le mode est en ecriture, alors le Pilote sera de nouveau disponible pour un nouvel
 * écrivain.
 *
 * */

int buf_release (struct inode *inode, struct file *filp);

/*
 * Fonction: buf_open
 *
 * Description: Sert a verifier le mode l'ouverture.
 * 	3 modes:
 * 		O_RDONLY : Read Only
 * 		O_WRONLY : Write Only
 * 		O_RDWR : Read/Write
 *
 * 		Note: Driver ne peut etre ouvert en mode write qu'une seule fois
 * 		Sinon, erreur "ENOTTY" sera retournee.
 */

int buf_open (struct inode *inode, struct file *filp);

/*
 * Fonction: buf_ioctl
 *
 * 	Description:
 * Permet d'envoyer des commandes au driver pour soit obtenir de l'info ou regler certains parametres.
 *
 * 	Fonctions:
 * 		GetNumData : lit le nombre de donnees actuellement dans le Buffer
 * 		GetNumReader: lit le nombre actuel de lecteurs
 * 		GetBufSize: lit la taille du buffer
 * 		SetBufSize: - Redimensionner le buffer, faire attention a bien placer les donnees dans la nouvelle structure.
 * 						reajuste les index "InIdx" et "OutIdx"
 * 					- Code d'erreur si l'ancien buffer est trop gros pour le nouveau (trop de donnees). Operation non completee.
 * 					- Erreur si le buffer n'est pas disponible. (voir SemBuf)
 * 					- Juste dispo pour admin (CAP_SYS_ADMIN). Donc user control.
 * */

long buf_ioctl (struct file *filp, unsigned int cmd, unsigned long arg);
