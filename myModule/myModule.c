/*
 * myModule.c
 *
 *  Created on: 2013-10-01
 *      Author: AJ52730
 */

//Includes et Declarations

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/fcntl.h>
#include <linux/wait.h>
#include <linux/spinlock.h>
#include <linux/device.h>
#include <linux/sched.h>
#include <linux/ioctl.h>

#include <asm/atomic.h>
#include <asm/uaccess.h>

#define READWRITE_BUFSIZE 16
#define DEFAULT_BUFSIZE 256

MODULE_LICENSE("Dual BSD/GPL");

struct class *my_class;
static int Major;
static int Minor;
static struct cdev *my_cdev;

#include "myModule.h"
#include "Buffer.c"
#include "FileOps.c"

module_init(buf_init);
module_exit(buf_exit);

int __init buf_init(void)
{
	int ret;
	char i = 0;
	//charger le pilote dans /dev/ avec alloc
	ret = alloc_chrdev_region(&(BDev.dev),0,1,"regAllocDriver");

	my_class = class_create(THIS_MODULE,"ETSELE");

	// va apparaitre dans /dev/etsele_cdev
	device_create(my_class, NULL, BDev.dev, &BDev, "etsele_cdev");

	Major = MAJOR(BDev.dev);
	Minor = MINOR(BDev.dev);

	my_cdev = cdev_alloc();
	my_cdev ->ops = &Buf_fops;
	my_cdev->owner = THIS_MODULE;

	cdev_add(my_cdev, BDev.dev, 1);
	sema_init(&(BDev.SemBuf),1);

	Buffer.BufSize = DEFAULT_BUFSIZE;
	Buffer.Buffer = kmalloc(Buffer.BufSize * sizeof(char), GFP_KERNEL);
	BDev.ReadBuf = kmalloc(READWRITE_BUFSIZE * sizeof(char), GFP_KERNEL);
	BDev.WriteBuf = kmalloc(READWRITE_BUFSIZE * sizeof(char), GFP_KERNEL);

	//Init pour donner des valeurs initiales.
	down_interruptible(&BDev.SemBuf);
	while(i<Buffer.BufSize)
	{
		BufIn(&Buffer, &i);
		i++;
	}

	up(&BDev.SemBuf);

	//on veut return le code d'erreur. Donné par device_create
	return ret;
}

void __exit buf_exit(void)
{
	kfree(BDev.ReadBuf);
	kfree(BDev.WriteBuf);
	kfree(Buffer.Buffer);
	unregister_chrdev_region(BDev.dev,1);
	device_destroy(my_class, BDev.dev);
	class_destroy(my_class);
	cdev_del(my_cdev);
}
