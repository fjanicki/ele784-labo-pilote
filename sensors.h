#ifndef _SENSORS_H_
#define _SENSORS_H_

#define DATASIZE 30

#define ACCEL_RATE		1
#define GYRO_RATE		1
#define SONAR_RATE		8
#define BAROM_RATE		2
#define MAGNETO_RATE	2

#define ACCEL_TRIG		0
#define GYRO_TRIG		0
#define SONAR_TRIG		0
#define BAROM_TRIG		1
#define MAGNETO_TRIG	1

#define ACCEL_POS		2
#define GYRO_POS		5
#define SONAR_POS		10
#define BAROM_POS		25
#define MAGNETO_POS		26

#define NUMSAMPLE_POS	1

#define INVALID_CHECKSUM -1
#define OLD_SAMPLE        0
#define NEW_SAMPLE        1

#define MAXNUMECH	65536L

#define DEV_MINOR		0x00
#define DEV_MINORS		0x01

/************************************************************************************************************************************/

enum { ACCELEROMETRE, GYROSCOPE, SONAR, BAROMETRE, MAGNETOMETRE };

struct nav_data {
    struct fasync_struct *async_queue; /* asynchronous readers */
	wait_queue_head_t	  wait;
	spinlock_t	    	  lock;
	struct semaphore 	  ReadMutex;
	atomic_t	    	  status;
	struct timespec		  timestamp;
	uint32_t	    	  ech_num;
	uint16_t	    	  type;
	uint16_t	    	  data[3];
};
 
struct read_data {
	int16_t		    status;
	struct timespec timestamp;
	uint32_t	    ech_num;
	uint16_t	    type;
	uint16_t	    data[3];
};

struct sensordev {
	dev_t			dev;
	struct cdev		cdev;
	struct class	*cclass;
	struct nav_data	*data;
};

static struct nav_data accelerometre= { .timestamp.tv_sec = 0, .timestamp.tv_nsec = 0, .ech_num = 0, .type = ACCELEROMETRE,	.data = { 0, 0, 0 } };
static struct nav_data gyroscope	= { .timestamp.tv_sec = 0, .timestamp.tv_nsec = 0, .ech_num = 0, .type = GYROSCOPE,		.data = { 0, 0, 0 } };
static struct nav_data sonar		= { .timestamp.tv_sec = 0, .timestamp.tv_nsec = 0, .ech_num = 0, .type = SONAR,			.data = { 0, 0, 0 } };
static struct nav_data barometre 	= { .timestamp.tv_sec = 0, .timestamp.tv_nsec = 0, .ech_num = 0, .type = BAROMETRE, 	.data = { 0, 0, 0 } };
static struct nav_data magnetometre	= { .timestamp.tv_sec = 0, .timestamp.tv_nsec = 0, .ech_num = 0, .type = MAGNETOMETRE, 	.data = { 0, 0, 0 } };

static struct read_data AccelData, GyroData, SonarData, BaromData, MagnetoData;

static struct sensordev accel_dev	= { .data = (struct nav_data *) &accelerometre };
static struct sensordev gyro_dev	= { .data = (struct nav_data *) &gyroscope };
static struct sensordev sonar_dev	= { .data = (struct nav_data *) &sonar };
static struct sensordev barom_dev	= { .data = (struct nav_data *) &barometre };
static struct sensordev magneto_dev = { .data = (struct nav_data *) &magnetometre };

static ssize_t      sensor_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos);
static int          sensor_open(struct inode *inode, struct file *filp);
static int          sensor_poll(struct file *filp, poll_table *wait);
static int          sensor_release(struct inode *inode, struct file *filp);
static int          sensor_fasync(int fd, struct file *filp, int mode);

static struct file_operations sensordev_fops = {
	.owner 	 = THIS_MODULE,
	.read	 = sensor_read,
	.poll	 = sensor_poll,
	.open	 = sensor_open,
	.release = sensor_release,
	.fasync	 = sensor_fasync
};

/************************************************************************************************************************************/

struct ktermios	configs = {
		.c_cflag  = 0x00001CB4,
		.c_iflag  = 0x00000000,
		.c_lflag  = 0x00000000,
		.c_oflag  = 0x00000004,
		.c_line   = 0x00,
		.c_cc     = {0x03, 0x1c, 0x7f, 0x15, 0x04, 0x00, 0x01, 0x00, 0x11, 0x13, 0x1a, 0x00, 0x12, 0x0f, 0x17, 0x16, 0x00, 0x00, 0x00},
		.c_ispeed = 460800,
		.c_ospeed = 460800
		};


static struct ttygrab_data {
	spinlock_t	lock_buf;
    struct timespec	timestamp;
	uint32_t	ech_num_offset;
	uint16_t	buffer[DATASIZE];
	struct nav_data	* navdata[5];
} TTYGrabData = { .navdata = { 	(struct nav_data	*) &accelerometre, 
								(struct nav_data	*) &gyroscope, 
								(struct nav_data	*) &sonar, 
								(struct nav_data	*) &barometre, 
								(struct nav_data	*) &magnetometre } };


static struct ttygrab {
	struct file			*f_tty;
	atomic_t			start;
	wait_queue_head_t	start_wait;
	atomic_t			new_sample;
	wait_queue_head_t	sample_wait;
	struct ttygrab_data	*data;
} ttygrab = { .data = (struct ttygrab_data *) &TTYGrabData };

static int 			ttygrab_start(void);
static int 			ttygrab_stop(void);
static int __init 	sensors_init(void);
static void __exit 	sensors_exit(void);

#endif

